#define R_NO_REMAP
#define STRICT_R_HEADERS
#include <Rinternals.h>
#include "vector_ops/api.h"

SEXP mutual_wrapper(
        SEXP lhs_array,
        SEXP rhs_array)
{

    return Rf_ScalarReal(
        mutual_information(
            REAL(lhs_array),
            Rf_length(lhs_array),
            REAL(rhs_array),
            Rf_length(rhs_array)));
}

static const R_CallMethodDef CallEntries[] = {
    {"mutual_wrapper", (DL_FUNC) &mutual_wrapper, 2},
    {NULL, NULL, 0}
};

void R_init_TestRustinR(DllInfo *dll) {
    R_registerRoutines(dll, NULL, CallEntries, NULL, NULL);
    R_useDynamicSymbols(dll, FALSE);
}
