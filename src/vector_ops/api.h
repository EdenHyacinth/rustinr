#include <stdint.h>
#include <math.h>

#ifdef _cplusplus
extern "C" {
#endif

double_t mutual_information(
    const double* lhs_array,
    int lhs_length,
    const double* rhs_array,
    int rhs_length
);

#ifdef __cplusplus
}
#endif
