extern crate libc;

mod vector_ops;

pub use vector_ops::mutual_information;
