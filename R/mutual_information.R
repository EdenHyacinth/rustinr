#### Assert ####
#' @title Assert that X is True
#' @description Provides the unevaluated statement if X is not true
#' @param x Statement that evaluates to a boolean
Assert <- function(x) {
  call <-
    gsub(x = match.call(),
         pattern = "Assert(.*)",
         replacement = "",
         1)[[2]]
  if (!x) {
    stop(paste0("Assert failed - ", call))
  }
}


#' @title Mutual Information
#' @description Returns how much information in bits is shared between the two variables provided. This should be symmetrical, and so
#' there is no concern about ordering.
#' @param target Variable to check regressor against
#' @param regressor - Variable of same length as target to be checked against target
#' @rdname mutual_information
#' @useDynLib rustinr mutual_wrapper
#' @importFrom
mutual_information <- function(target, regressor) {
  Assert(length(target) == length(regressor))
  Assert(sd(target) > 0)
  Assert(sd(regressor) > 0)
  Assert(!identical(target, regressor))
  .Call(mutual_wrapper, as.double(target), as.double(regressor))
}
